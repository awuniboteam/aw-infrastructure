package aw4agents;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Semaphore;

import aw4agents.exceptions.ServiceConnectorUnactiveException;
import cartago.Artifact;
import cartago.GUARD;
import cartago.INTERNAL_OPERATION;

public class AwArtifact extends Artifact{
	protected String awName;
	
	private volatile Map<String, MultipleAwaitSession> sessions = new HashMap<>();

	protected void startMultipleAwaitSession(String id, int n) {
		sessions.put(id, new MultipleAwaitSession(n));
	}
	
	protected boolean endMultipleAwaitSession(String id) {
		await("guard", id);
		
		boolean success = !sessions.get(id).errorRegistered();
		
		sessions.remove(id);
		
		return success;
	}
	
	protected void notifyMultipleAwaitSession(String id) {
		execInternalOp("notifyRelease", id);
	}
	
	protected void notifyErrorInMultipleAwaitSession(String id) {
		sessions.get(id).registerError();
		execInternalOp("notifyRelease", id);
	}
	
	@GUARD boolean guard(String id) {
		return sessions.get(id).completed();
	}
	
	@INTERNAL_OPERATION void notifyRelease(String id) {
		sessions.get(id).release();
	}
	
	//----------------------------------------------------------------
	
	protected void observeAEProperty(String obsProperty, String id, String entityProperty) {		
		new Thread(() ->  {
			while(true) {
				try {
					ServiceConnector.REST.doGET("/aw/" + awName + "/entities/" + id + "/properties/" + entityProperty, 1000000, responseHandler -> {
						if(responseHandler.statusCode() == 200) {
							responseHandler.bodyHandler(data -> {
								execInternalOp("notifyNewPropertyValue", obsProperty, data);
							});
						}
						
//						responseHandler.exceptionHandler(exceptionHandler -> {
//							log("------>" + exceptionHandler.getMessage());
//						});
					});
				} catch (ServiceConnectorUnactiveException e) {
					e.printStackTrace();
				}
				
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}).start();	
	}
	
	@INTERNAL_OPERATION void notifyNewPropertyValue(String id, Object value) {
		updateObsProperty(id, value);
	}
}

class MultipleAwaitSession {
	private volatile boolean error;
	private volatile Semaphore s;
	
	public MultipleAwaitSession(int n) {
		error = false;
		s = new Semaphore(-n);
	}
	
	public boolean completed() {
		return s.availablePermits() >= 0;
	}
	
	public boolean errorRegistered() {
		return error;
	}
	
	public void registerError() {
		error = true;
	}
	
	public void release() {
		s.release();
	}
}
