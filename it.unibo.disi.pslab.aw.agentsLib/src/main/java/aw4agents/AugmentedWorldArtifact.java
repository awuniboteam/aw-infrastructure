package aw4agents;

import aw4agents.exceptions.ServiceConnectorUnactiveException;

import cartago.INTERNAL_OPERATION;
import cartago.OPERATION;
import cartago.OpFeedbackParam;

import io.vertx.core.json.JsonObject;

public final class AugmentedWorldArtifact extends AwArtifact {

	private static final String AW_RUNNING_OBS_PROP = "awRunning";
	
	private volatile String awName;
	
	void init() {
		defineObsProperty(AW_RUNNING_OBS_PROP, false);
	}
	
	@OPERATION public void runAW(String name) {
		runAW(name, null, null, null);
	}
	
	@OPERATION public void runAW(String name, String environment, String referenceSystem) {
		runAW(name, environment, referenceSystem, null);
	}
	
	@OPERATION public void runAW(String name, String environment, String referenceSystem, String markersDb) {
		this.awName = name;
		
		JsonObject body = new JsonObject();
		
		if(environment != null) {
			body.put("environment", environment); //"ar" OR "vr"
		} else {
			body.put("environment", "vr");
		}
		
		if(referenceSystem != null) {
			body.put("reference-system", referenceSystem); //"default" OR "marker-based"
		} else {
			body.put("reference-system", "default");
		}
				
		if(markersDb != null) {
			body.put("markers-db", markersDb);
		}
		
		try {
			ServiceConnector.REST.doPUT("/aw/" + awName, body, 10000, responseHandler -> {
				if(responseHandler.statusCode() == 200) {
					execInternalOp("notifyAWCreation");
				} else {
					log("error in AW creation!");
				}
			});
		} catch (ServiceConnectorUnactiveException e) {
			e.printStackTrace();
		}
	}
	
	@OPERATION public void getName(OpFeedbackParam<String> name) {
		name.set(awName);
	}
	
	@INTERNAL_OPERATION public void notifyAWCreation() {
		updateObsProperty(AW_RUNNING_OBS_PROP, true);
	}
}
