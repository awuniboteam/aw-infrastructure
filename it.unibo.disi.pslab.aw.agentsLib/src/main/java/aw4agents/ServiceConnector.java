package aw4agents;

import aw4agents.exceptions.ServiceConnectorUnactiveException;

import cartago.Artifact;
import cartago.INTERNAL_OPERATION;
import cartago.OPERATION;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientOptions;
import io.vertx.core.http.HttpClientRequest;
import io.vertx.core.http.HttpClientResponse;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.json.JsonObject;

public class ServiceConnector extends Artifact{
	
	private static final String SERVICE_CONNECTED_OBS_PROP = "serviceConnected";
	
	private static HttpClient client = null;
	
	void init(){
		defineObsProperty(SERVICE_CONNECTED_OBS_PROP, false);	
	}
	
	@OPERATION public void activate(String ip, int port) {
		Vertx vertx = Vertx.vertx();
	
		client = vertx.createHttpClient(new HttpClientOptions().setDefaultHost(ip).setDefaultPort(port));
		
		client.get("/aw/service/info").handler(responseHandler -> {
			if(responseHandler.statusCode() == 200) {
				responseHandler.bodyHandler(data -> {
					log("AW Service Informations\n" + data.toString());
					execInternalOp("notifyServiceConnected");
				});
			} else {
				log("BAD REQUEST: Error in get service info request!");
			}
		}).setTimeout(5000).end();
	}
	
	@INTERNAL_OPERATION public void notifyServiceConnected() {
		updateObsProperty(SERVICE_CONNECTED_OBS_PROP, true);
	}
	
	/**
	 *
	 */
	public static enum RESTHeader {
		APPLICATION_JSON("content-type", "application/json");
		
		private String name, value;
		
		RESTHeader(String name, String value) {
			this.name = name;
			this.value = value;
		}
		
		public String getName() {
			return name;
		}
		
		public String getValue() {
			return value;
		}
	}
	
	/**
	 * 
	 */
	public static class REST {
		
		private static void internalGET(String host, int port, String requestURI, long timeout, RESTHeader restHeader, Handler<HttpClientResponse> responseHandler, Handler<Throwable> exceptionHandler) throws ServiceConnectorUnactiveException {
			if(client != null) {
				HttpClientRequest request;
				
				if(host != null) {
					request = client.get(port, host, requestURI);
				} else {
					request = client.get(requestURI);
				}
				
				request.setTimeout(timeout);
				
				if(restHeader != null) {
					request.putHeader(restHeader.getName(), restHeader.getValue());
				}
				
				if(responseHandler != null) {
					request.handler(responseHandler);
				}
				
				if(exceptionHandler != null) {
					request.exceptionHandler(exceptionHandler);
				}
				
			} else {
				throw new ServiceConnectorUnactiveException();
			}
		}
		
		/**
		 * 
		 * @param requestURI
		 * @param timeout
		 * @param responseHandler
		 * @throws ServiceConnectorUnactiveException
		 */
		public static void doGET(String requestURI, long timeout, Handler<HttpClientResponse> responseHandler) throws ServiceConnectorUnactiveException {
			internalGET(null, 0, requestURI, timeout, null, responseHandler, null);
		}
		
		/**
		 * 
		 * @param requestURI
		 * @param timeout
		 * @param responseHandler
		 * @param exceptionHandler
		 * @throws ServiceConnectorUnactiveException
		 */
		public static void doGET(String requestURI, long timeout, Handler<HttpClientResponse> responseHandler, Handler<Throwable> exceptionHandler) throws ServiceConnectorUnactiveException {
			internalGET(null, 0, requestURI, timeout, null, responseHandler, exceptionHandler);
		}
		
		/**
		 * 
		 * @param requestURI
		 * @param timeout
		 * @param header
		 * @param responseHandler
		 * @throws ServiceConnectorUnactiveException
		 */
		public static void doGET(String requestURI, long timeout, RESTHeader header, Handler<HttpClientResponse> responseHandler) throws ServiceConnectorUnactiveException {
			internalGET(null, 0, requestURI, timeout, header, responseHandler, null);
		}
		
		/**
		 * 
		 * @param requestURI
		 * @param timeout
		 * @param header
		 * @param responseHandler
		 * @param exceptionHandler
		 * @throws ServiceConnectorUnactiveException
		 */
		public static void doGET(String requestURI, long timeout, RESTHeader header, Handler<HttpClientResponse> responseHandler, Handler<Throwable> exceptionHandler) throws ServiceConnectorUnactiveException {
			internalGET(null, 0, requestURI, timeout, header, responseHandler, exceptionHandler);
		}
		
		/**
		 * 
		 * @param host
		 * @param port
		 * @param requestURI
		 * @param timeout
		 * @param responseHandler
		 * @throws ServiceConnectorUnactiveException
		 */
		public static void doGET(String host, int port, String requestURI, long timeout, Handler<HttpClientResponse> responseHandler) throws ServiceConnectorUnactiveException {
			internalGET(host, port, requestURI, timeout, null, responseHandler, null);
		}
		
		/**
		 * 
		 * @param host
		 * @param port
		 * @param requestURI
		 * @param timeout
		 * @param responseHandler
		 * @param exceptionHandler
		 * @throws ServiceConnectorUnactiveException
		 */
		public static void doGET(String host, int port, String requestURI, long timeout, Handler<HttpClientResponse> responseHandler, Handler<Throwable> exceptionHandler) throws ServiceConnectorUnactiveException {
			internalGET(host, port, requestURI, timeout, null, responseHandler, exceptionHandler);
		}
		
		/**
		 * 
		 * @param host
		 * @param port
		 * @param requestURI
		 * @param timeout
		 * @param header
		 * @param responseHandler
		 * @throws ServiceConnectorUnactiveException
		 */
		public static void doGET(String host, int port, String requestURI, long timeout, RESTHeader header, Handler<HttpClientResponse> responseHandler) throws ServiceConnectorUnactiveException {
			internalGET(host, port, requestURI, timeout, header, responseHandler, null);
		}
		
		/**
		 * 
		 * @param host
		 * @param port
		 * @param requestURI
		 * @param timeout
		 * @param header
		 * @param responseHandler
		 * @param exceptionHandler
		 * @throws ServiceConnectorUnactiveException
		 */
		public static void doGET(String host, int port, String requestURI, long timeout, RESTHeader header, Handler<HttpClientResponse> responseHandler, Handler<Throwable> exceptionHandler) throws ServiceConnectorUnactiveException {
			internalGET(host, port, requestURI, timeout, header, responseHandler, exceptionHandler);
		}
		
		public static void doPOST(String address, JsonObject requestBody, long timeout, Handler<HttpClientResponse> responseHandler) throws ServiceConnectorUnactiveException {
			if(client != null) {
				Buffer buffer = Buffer.buffer("");
				
				if(requestBody != null) {
					buffer = Buffer.buffer(requestBody.encode());
				}
								
				client.post(address).handler(responseHandler)
				.setTimeout(timeout)
				.putHeader("content-type", "application/json")
				.putHeader(HttpHeaders.CONTENT_LENGTH, buffer.length() + "").write(buffer)
				.end();
			} else {
				throw new ServiceConnectorUnactiveException();
			}
		}
		
		public static void doPUT(String address, JsonObject requestBody, long timeout, Handler<HttpClientResponse> responseHandler) throws ServiceConnectorUnactiveException {
			if(client != null) {
				Buffer buffer = Buffer.buffer("");
				
				if(requestBody != null) {
					buffer = Buffer.buffer(requestBody.encode());
				}
				
				client.put(address).handler(responseHandler)
				.setTimeout(timeout)
				.putHeader("content-type", "application/json")
				.putHeader(HttpHeaders.CONTENT_LENGTH, buffer.length() + "").write(buffer)
				.end();
			} else {
				throw new ServiceConnectorUnactiveException();
			}
		}
		
		public static void doPUT(String host, int port, String address, JsonObject requestBody, long timeout, Handler<HttpClientResponse> responseHandler) throws ServiceConnectorUnactiveException {
			if(client != null) {
				Buffer buffer = Buffer.buffer("");
				
				if(requestBody != null) {
					buffer = Buffer.buffer(requestBody.encode());
				}
				
				client.put(port, host, address).handler(responseHandler)
				.setTimeout(timeout)
				.putHeader("content-type", "application/json")
				.putHeader(HttpHeaders.CONTENT_LENGTH, buffer.length() + "").write(buffer)
				.end();
			} else {
				throw new ServiceConnectorUnactiveException();
			}
		}
		
		public static void doDELETE() {
			//TODO: to be implemented
		}
	}
}
