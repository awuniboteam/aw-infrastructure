package it.unibo.disi.pslab.aw;

public class Config {
	public static boolean UNITY_ENGINE_ENABLED;
	public static String UNITY_ENGINE_ADDRESS;
	public static int UNITY_ENGINE_PORT;
	public static int UNITY_ENGINE_SEND_ATTEMPTS;
	
	public static String ENTITIES_PATH;
}
