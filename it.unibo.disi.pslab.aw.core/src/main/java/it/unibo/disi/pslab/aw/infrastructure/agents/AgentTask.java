package it.unibo.disi.pslab.aw.infrastructure.agents;

public interface AgentTask extends Runnable{
	void interrupt();
}
