package it.unibo.disi.pslab.aw.infrastructure.enumerates;

public enum AwEnvironment {
	VR("vr"),
	AR("ar");
	
	private String description;
	
	AwEnvironment(String description) {
		this.description = description;
	}
	
	@Override
	public String toString() {
		return this.description;
	}
}
