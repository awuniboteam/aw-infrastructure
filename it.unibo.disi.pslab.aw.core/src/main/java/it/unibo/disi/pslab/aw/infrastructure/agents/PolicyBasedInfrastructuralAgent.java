package it.unibo.disi.pslab.aw.infrastructure.agents;

import java.util.HashMap;
import java.util.Map;
import io.vertx.core.json.JsonObject;
import it.unibo.disi.pslab.aw.infrastructure.exceptions.PolicyMalformedException;

public abstract class PolicyBasedInfrastructuralAgent extends InfrastructuralAgent{

	private int policyCounter = 0;
	
	private Map<String, AgentTask> activePolicies = new HashMap<>();
		
	public abstract JsonObject registerNewPolicy(JsonObject policy) throws PolicyMalformedException;

	public synchronized void removePolicy(String policyId) {
		if(activePolicies.containsKey(policyId)) {
			activePolicies.get(policyId).interrupt();
			activePolicies.remove(policyId);
		}
	}
	
	public final int numberOfActivePolicies() {
		return activePolicies.size();
	}
	
	protected final synchronized String getNextAvailablePolicyId() {
		return "policy" + (policyCounter++);
	}
	
	protected final synchronized void executeTask(AgentTask task, String policyId) {
		activePolicies.put(policyId, task);
		executeTask(task);
	}
}
