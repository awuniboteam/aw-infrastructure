package it.unibo.disi.pslab.aw.infrastructure.agents;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public abstract class InfrastructuralAgent {

	private ExecutorService agentExecutor;
	private boolean active = false;
	
	public final void start() {
		agentExecutor = Executors.newCachedThreadPool();
		active = true;
	}
	
	public final void stop() {
		agentExecutor.shutdown();
		active = false;
	}
	
	public final boolean isActive() {
		return active;
	}
	
	protected synchronized void executeTask(AgentTask task) {
		agentExecutor.submit(task);
	}
}
