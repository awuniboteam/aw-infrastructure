package it.unibo.disi.pslab.aw.infrastructure;

import java.util.ArrayList;
import java.util.List;

import it.unibo.disi.pslab.aw.ontology.AW;

public class QueuesManager {

	private static int queueCnt = 0;
	
	public static synchronized String getNextAvailableQueueId() {
		return "queue" + (queueCnt++);
	}
	
	public static synchronized void storeMessage(String queueId, Message m) {
		MessageQueue queue = getQueueById(queueId);
		
		if(queue == null) {
			queue = new MessageQueue(queueId);
			AW.instance().messageQueues().add(queue);
		}
		
		queue.pushMessage(m);
	}
	
	public static synchronized Message consumeMessage(String queueId) {
		MessageQueue queue = getQueueById(queueId);
		
		if(queue != null) {
			return queue.popMessage();		
		} else {
			//TODO: queue not found!
			return null;
		}
	}
	
	public static synchronized List<Message> getMessageListByQueue(String queueId) {
		MessageQueue queue = getQueueById(queueId);
		
		if(queue != null) {
			return queue.messages();			
		} else {
			//TODO: queue not found!
			return new ArrayList<>();
		}
	}
	
	private static MessageQueue getQueueById(String queueId) {
		MessageQueue queue = null;
		
		for(MessageQueue q : AW.instance().messageQueues()) {
			if(q.id().equals(queueId)) {
				queue = q;
			}
		}
		
		return queue;
	}
}
