package it.unibo.disi.pslab.aw.infrastructure;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class MessageQueue {

	private String id;
	
	private Queue<Message> messages;
	
	public MessageQueue(String id) {
		this.id = id;
		
		messages = new LinkedList<>();
	}
	
	public String id() {
		return this.id;
	}
	
	public boolean isEmpty() {
		return messages.isEmpty();
	}
	
	public int messagePendings() {
		return messages.size();
	}
	
	public void pushMessage(Message m) {
		messages.add(m);
	}
	
	public Message popMessage() {
		return messages.poll();
	}
	
	public List<Message> messages(){
		List<Message> list = new ArrayList<>();
		
		messages.forEach(m -> {
			list.add(m);
		});
		
		return list;
	}
}
