package it.unibo.disi.pslab.aw.infrastructure;

import java.util.Date;

import io.vertx.core.json.JsonObject;

public class Message {
	
	private String type;
	private String timestamp;
	private JsonObject content;
	
	public Message(String type, JsonObject content) {
		this.type = type;
		this.timestamp = new Date().toString();
		this.content = content;
	}
	
	public String type() {
		return this.type;
	}
	
	public String timestamp() {
		return this.timestamp;
	}
	
	public JsonObject content() {
		return this.content;
	}
}
