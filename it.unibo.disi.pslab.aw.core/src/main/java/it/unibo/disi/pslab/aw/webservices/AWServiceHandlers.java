package it.unibo.disi.pslab.aw.webservices;

import java.util.ArrayList;
import java.util.List;

import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.DecodeException;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.RoutingContext;

import it.unibo.disi.pslab.aw.Config;
import it.unibo.disi.pslab.aw.infrastructure.Message;
import it.unibo.disi.pslab.aw.infrastructure.QueuesManager;
import it.unibo.disi.pslab.aw.infrastructure.agents.CollisionDetectorAgent;
import it.unibo.disi.pslab.aw.infrastructure.exceptions.PolicyMalformedException;
import it.unibo.disi.pslab.aw.net.UnityEngine;
import it.unibo.disi.pslab.aw.ontology.AE;
import it.unibo.disi.pslab.aw.ontology.AEFactory;
import it.unibo.disi.pslab.aw.ontology.AW;
import it.unibo.disi.pslab.aw.ontology.ae.AngularOrientation;
import it.unibo.disi.pslab.aw.ontology.ae.BasicExtension;
import it.unibo.disi.pslab.aw.ontology.ae.CartesianLocation;
import it.unibo.disi.pslab.aw.ontology.ae.Extension;
import it.unibo.disi.pslab.aw.ontology.ae.GpsLocation;
import it.unibo.disi.pslab.aw.ontology.ae.Location;
import it.unibo.disi.pslab.aw.ontology.ae.Orientation;
import it.unibo.disi.pslab.aw.ontology.ae.SphericExtension;
import it.unibo.disi.pslab.aw.ontology.exceptions.ActionNotFoundException;
import it.unibo.disi.pslab.aw.ontology.exceptions.AugmentedEntityCreationException;
import it.unibo.disi.pslab.aw.ontology.exceptions.AugmentedEntityNotFoundException;
import it.unibo.disi.pslab.aw.ontology.exceptions.AugmentedWorldAlreadyStartedException;
import it.unibo.disi.pslab.aw.ontology.exceptions.PropertyNotFoundException;
import it.unibo.disi.pslab.aw.ontology.physical.PhysicalThingInterface;

public class AWServiceHandlers {	
	protected static void handleGetServiceInfo(RoutingContext rc) {
		HttpServerResponse response = rc.response();

		JsonObject unityObj = new JsonObject().put("connected", UnityEngine.isConnected());

		if (UnityEngine.isConnected()) {
			unityObj.put("ip", Config.UNITY_ENGINE_ADDRESS);
			unityObj.put("port", Config.UNITY_ENGINE_PORT);
		}
		
		JsonObject agentsObj = new JsonObject()
				.put(CollisionDetectorAgent.class.getSimpleName(), new JsonObject()
						.put("active", CollisionDetectorAgent.instance().isActive())
					    .put("monitoredPolicies", CollisionDetectorAgent.instance().numberOfActivePolicies()));

		JsonObject responseBody = new JsonObject()
				.put("service-version", C.Service.VERSION)
				.put("unity-engine", unityObj)
				.put("infrastructural-agents", agentsObj);

		sendOK(responseBody, response);
	}

	protected static void handleCreateAW(RoutingContext rc) {
		String name = rc.request().getParam("name");
		
		HttpServerResponse response = rc.response();
		JsonObject body = rc.getBodyAsJson();
		
		if (name != null) {
			if(checkRequestBody("createAw", body)) {
				try {
					AW.instance().start(name, body);
					sendOK(response);
					log("AW created successfully! " + AW.instance().getJSONRepresentation().encode());
				} catch (AugmentedWorldAlreadyStartedException e) {
					sendError(HttpStatus.CONFLICT, "CONFLICT: An augmented world is already running on the infrastructure!", response);
				}
			} else {
				sendError(HttpStatus.BAD_REQUEST, "BAD REQUEST: Errors in request body.", response);
			}
			
		} else {
			sendError(HttpStatus.BAD_REQUEST, response);
		}
	}
	
	protected static void handleGetAWInfo(RoutingContext rc) {
		String name = rc.request().getParam("name");

		HttpServerResponse response = rc.response();

		if(badAwRequest(name, response)) {
			return;
		}
		
		JsonArray entities = new JsonArray();

		AW.instance().entities().forEach(ae -> {
			entities.add(ae.id());
		});

		JsonObject runningAW = new JsonObject()
				.put("active-aw", AW.instance().getJSONRepresentation())
				.put("active-entities", entities);

		sendOK(runningAW, response);
	}

	protected static void handleGetEntities(RoutingContext rc) {

		String awID = rc.request().getParam("awID");
		
		HttpServerResponse response = rc.response();
		
		if(badAwRequest(awID, response)) {
			return;
		}
		
		JsonArray entities = new JsonArray();

		AW.instance().entities().forEach(ae -> {
			entities.add(ae.id());
		});
		
		sendOK(new JsonObject().put("active-entities", entities), response);
	}

	protected static void handleAddAugmentedEntity(RoutingContext rc) {
		String awID = rc.request().getParam("awID");
		
		HttpServerResponse response = rc.response();
		
		if(badAwRequest(awID, response)) {
			return;
		}

		JsonObject descriptionObj = rc.getBodyAsJson();
		
		if(descriptionObj == null) {
			sendError(HttpStatus.BAD_REQUEST, "BAD REQUEST: Errors in request body.", response);
			return;
		}

		AE ae = null;

		try {
			ae = AEFactory.createAugmentedEntity(rc.request().getParam("type"), descriptionObj);
		} catch (AugmentedEntityCreationException e) {
			log("Unable to create requested Augmented Entity");
			sendError(HttpStatus.BAD_REQUEST, "BAD REQUEST: Errors in  type or request body content.", response);
			return;
		}

		AW.instance().addAugmentedEntity(ae);

		sendOK(response);

		log("New AE created and added to the AW! <" + ae.getJSONRepresentation().encode() + ">");
		
		if(ae instanceof PhysicalThingInterface && !ae.things().isEmpty()) {
			for(Object thing : ae.things()) {
				AEFactory.linkToPhysicalThing(ae, (JsonObject) thing);
			}
		}
	}

	protected static void handleGetEntityByID(RoutingContext rc) {
		String awID = rc.request().getParam("awID");
		
		HttpServerResponse response = rc.response();
		
		if(badAwRequest(awID, response)) {
			return;
		}
		
		String entityID = rc.request().getParam("entityID");

		if (entityID == null) {
			sendError(HttpStatus.BAD_REQUEST, response);
			return;
		}	
		
		try {
			AE ae = AW.instance().entity(entityID);
			sendOK(ae.getJSONRepresentation(), response);
		} catch (AugmentedEntityNotFoundException e) {
			sendError(HttpStatus.NOT_FOUND, response);
		}
	}

	protected static void handleRemoveEntityByID(RoutingContext rc) {
		String awID = rc.request().getParam("awID");
		
		HttpServerResponse response = rc.response();
		
		if(badAwRequest(awID, response)) {
			return;
		}
		
		String entityID = rc.request().getParam("entityID");

		if (entityID == null) {
			sendError(HttpStatus.BAD_REQUEST, response);
			return;
		}
		
		try {
			AW.instance().removeAugmentedEntity(entityID);
			sendOK(response);
		} catch (AugmentedEntityNotFoundException e) {
			sendError(HttpStatus.NOT_FOUND, response);
		}
	}

	protected static void handleGetEntityModel(RoutingContext rc) {
		String awID = rc.request().getParam("awID");
		
		HttpServerResponse response = rc.response();
		
		if(badAwRequest(awID, response)) {
			return;
		}
		
		String entityID = rc.request().getParam("entityID");

		if (entityID == null) {
			sendError(HttpStatus.BAD_REQUEST, response);
			return;
		}
		
		try {
			AE ae = AW.instance().entity(entityID);

			JsonObject resBody = new JsonObject()
				.put("id", ae.id())
				.put("tag", ae.tag())
				.put("type", ae.type());

			sendOK(resBody, response);
		} catch (AugmentedEntityNotFoundException e) {
			sendError(HttpStatus.NOT_FOUND, response);
		}
	}

	protected static void handleGetEntityModelElement(RoutingContext rc) {
		String awID = rc.request().getParam("awID");
		
		HttpServerResponse response = rc.response();
		
		if(badAwRequest(awID, response)) {
			return;
		}
		
		String entityID = rc.request().getParam("entityID");
		String element = rc.request().getParam("modelElement");

		if (entityID == null) {
			sendError(HttpStatus.BAD_REQUEST, response);
			return;
		}
		
		try {
			AE ae = AW.instance().entity(entityID);

			switch(element) {
				case "id":
					sendOK("\"" + ae.id() + "\"", response);
					break;
					
				case "tag":
					sendOK("\"" + ae.tag() + "\"", response);
					break;
				
				case "type":
					sendOK("\"" + ae.type() + "\"", response);
					break;
				
				default:
					sendError(400, response);
					break;
			}
		} catch (AugmentedEntityNotFoundException e) {
			sendError(HttpStatus.NOT_FOUND, response);
		}
	}

	protected static void handleGetEntityProperties(RoutingContext rc) {
		String awID = rc.request().getParam("awID");
		
		HttpServerResponse response = rc.response();
		
		if(badAwRequest(awID, response)) {
			return;
		}
		
		String entityID = rc.request().getParam("entityID");

		if (entityID == null) {
			sendError(HttpStatus.BAD_REQUEST, response);
			return;
		}
		
		try {
			AE ae = AW.instance().entity(entityID);

			JsonObject resBody = new JsonObject()
					.put("location", ae.location().getJSONRepresentation())
					.put("orientation", ae.orientation().getJSONRepresentation())
					.put("extension", ae.extension().getJSONRepresentation())
					.put("holograms", ae.holograms());

			for (String p : ae.customProperties()) {
				try {
					try {
						resBody.put(p, ae.customProperty(p));
					} catch (IllegalStateException e) {
						resBody.put(p, JsonObject.mapFrom(ae.customProperty(p)));
					}
				} catch (PropertyNotFoundException e) {
					e.printStackTrace();
				}
			}
			
			sendOK(resBody, response);
		} catch (AugmentedEntityNotFoundException e) {
			sendError(HttpStatus.NOT_FOUND, response);
			return;
		}
	}

	protected static void handleGetEntityProperty(RoutingContext rc) {
		String awID = rc.request().getParam("awID");
		
		HttpServerResponse response = rc.response();
		
		if(badAwRequest(awID, response)) {
			return;
		}
		
		String entityID = rc.request().getParam("entityID");
		String property = rc.request().getParam("property");

		if (entityID == null || property == null) {
			sendError(HttpStatus.BAD_REQUEST, response);
			return;
		}
		
		try {
			AE ae = AW.instance().entity(entityID);

			if (property.equals("location")) {
				sendOK(ae.location().getJSONRepresentation(), response);
				return;
			}

			if (property.equals("orientation")) {
				sendOK(ae.orientation().getJSONRepresentation(), response);
				return;
			}

			if (property.equals("extension")) {
				sendOK(ae.extension().getJSONRepresentation(), response);
				return;
			}

			if (property.equals("holograms")) {
				sendOK(ae.holograms().encodePrettily(), response);
				return;
			}

			String res = "";

			try {
				res = JsonObject.mapFrom(ae.customProperty(property)).encodePrettily();
			} catch (IllegalArgumentException e) {
				if (ae.customProperty(property).getClass().equals(String.class)) {
					res = "\"" + String.valueOf(ae.customProperty(property)) + "\"";
				} else {
					res = String.valueOf(ae.customProperty(property));
				}
			}

			sendOK(res, response);
		} catch (AugmentedEntityNotFoundException | PropertyNotFoundException e) {
			sendError(HttpStatus.NOT_FOUND, response);
		}
	}

	protected static void handleSetEntityProperty(RoutingContext rc) {
		String awID = rc.request().getParam("awID");
		
		HttpServerResponse response = rc.response();
		
		if(badAwRequest(awID, response)) {
			return;
		}
		
		String entityID = rc.request().getParam("entityID");
		String property = rc.request().getParam("property");

		if (entityID == null || property == null) {
			sendError(HttpStatus.BAD_REQUEST, response);
			return;
		}
		
		try {
			AE ae = AW.instance().entity(entityID);

			if (property.equals("location")) {
				JsonObject details = rc.getBodyAsJson();
				
				Location location = null;

				if (details.getString("type").equals(Location.Type.CARTESIAN.toString())) {
					location = new CartesianLocation(details.getDouble("x"), details.getDouble("y"),
							details.getDouble("z"));
				} else if (details.getString("type").equals(Location.Type.GPS.toString())) {
					location = new GpsLocation(details.getDouble("latitude"), details.getDouble("longitude"),
							details.getDouble("altitude"));
				}

				if (location != null) {
					ae.location(location);
				}

				log("Location updated on " + ae.id());
				sendOK(response);
				return;
			}

			if (property.equals("orientation")) {
				JsonObject details = rc.getBodyAsJson();
				
				Orientation orientation = null;

				if (details.getString("type").equals(Orientation.Type.ANGULAR.toString())) {
					orientation = new AngularOrientation(details.getDouble("roll"), details.getDouble("pitch"),
							details.getDouble("yaw"));
				}

				if (orientation != null) {
					ae.orientation(orientation);
				}

				log("Orientation updated on " + ae.id());
				sendOK(response);
				return;
			}

			if (property.equals("extension")) {
				JsonObject details = rc.getBodyAsJson();
				
				Extension extension = null;
				
				if (details.getString("type").equals(Extension.Type.BASIC.toString())) {
					extension = new BasicExtension(details.getDouble("radius"));
				}
				
				if (details.getString("type").equals(Extension.Type.SPHERIC.toString())) {
					extension = new SphericExtension(details.getDouble("radius"));
				}
				
				if (extension != null) {
					ae.extension(extension);
				}

				log("Orientation updated on " + ae.id());
				
				sendOK(response);
				return;
			}
			
			if(ae.customProperties().contains(property)) {
				String details = rc.getBodyAsString();				
				try {
					//TODO: considerare i valori non string...
					ae.customProperty(property, details);
				} catch (PropertyNotFoundException e) {
					e.printStackTrace();
				} 
				
				sendOK(response);
				return;
			}

			log("error!");
			
			//TODO: to be completed. custom properties?
			
		} catch (AugmentedEntityNotFoundException e) {
			log("Entity or Property not found!");
			sendError(HttpStatus.NOT_FOUND, response);
		}
	}

	protected static void handleDoActionOnEntityHologram(RoutingContext rc) {
		String awID = rc.request().getParam("awID");
		
		HttpServerResponse response = rc.response();
		
		if(badAwRequest(awID, response)) {
			return;
		}
		
		String entityID = rc.request().getParam("entityID");
		String hologramID = rc.request().getParam("hologramID");
		String action = rc.request().getParam("action");

		if (entityID == null || hologramID == null || action == null) {
			sendError(HttpStatus.BAD_REQUEST, response);
		} else {
			JsonObject paramsObj = new JsonObject();

			try {
				paramsObj = rc.getBodyAsJson();
			} catch (DecodeException de) {	}

			if(UnityEngine.isEnabled()) {
				UnityEngine.executeHologramAction(entityID, hologramID, action, paramsObj);
			}

			sendOK(response);
		}
	}

	protected static void handleGetEntityActions(RoutingContext rc) {
		String awID = rc.request().getParam("awID");
		
		HttpServerResponse response = rc.response();
		
		if(badAwRequest(awID, response)) {
			return;
		}
		
		String entityID = rc.request().getParam("entityID");

		if (entityID == null) {
			sendError(HttpStatus.BAD_REQUEST, response);
			return;
		}
		
		try {
			AE ae = AW.instance().entity(entityID);

			JsonArray actionsList = new JsonArray();

			for (String action : ae.actions().keySet()) {
				JsonArray types = new JsonArray();

				for (int i = 0; i < ae.actions().get(action).length; i++) {
					types.add(ae.actions().get(action)[i]);
				}

				actionsList.add(new JsonObject().put("name", action).put("paramsTypeList", types));
			}

			sendOK(actionsList.encodePrettily(), response);

		} catch (AugmentedEntityNotFoundException e) {
			sendError(HttpStatus.NOT_FOUND, response);
		}
	}

	protected static void handleDoActionOnEntity(RoutingContext rc) {
		String awID = rc.request().getParam("awID");
		
		HttpServerResponse response = rc.response();
		
		if(badAwRequest(awID, response)) {
			return;
		}
		
		String entityID = rc.request().getParam("entityID");
		String action = rc.request().getParam("action");

		if (entityID == null || action == null) {
			sendError(HttpStatus.BAD_REQUEST, response);
		} else {
			try {
				List<Object> paramsValues = new ArrayList<>();

				try {
					JsonObject paramsObj = rc.getBodyAsJson();

					if (paramsObj != null) {
						JsonArray params = paramsObj.getJsonArray("params");

						if (params != null) {
							params.forEach(p -> paramsValues.add(p));
						}
					}
				} catch (DecodeException de) {
					//Action with no parameters. Nothing to do!
				}

				AE ae = AW.instance().entity(entityID);

				if (paramsValues.size() > 0) {
					ae.executeAction(ae, action, paramsValues.toArray());
				} else {
					ae.executeAction(ae, action);
				}

				sendOK(response);
			} catch (AugmentedEntityNotFoundException | ActionNotFoundException e) {
				sendError(HttpStatus.NOT_FOUND, response);
			}
		}
	}
	
	protected static void handleMessageQueueStatus(RoutingContext rc) {
		String awID = rc.request().getParam("awID");
		
		HttpServerResponse response = rc.response();
		
		if(badAwRequest(awID, response)) {
			return;
		}
		
		String queueId = rc.request().getParam("queueID");

		if (queueId == null) {
			sendError(400, response);
		} else {
			JsonArray messages = new JsonArray();
			
			List<Message> messageList = QueuesManager.getMessageListByQueue(queueId);
			
			messageList.forEach(m -> {
				messages.add(new JsonObject()
						.put("timestamp", m.timestamp())
						.put("type", m.type())
						.put("content", m.content()));
			});
			
			response.putHeader("content-type", "application/json").end(messages.encodePrettily());
		}
	}
	
	protected static void handlePopMessageFromQueue(RoutingContext rc) {
		String awID = rc.request().getParam("awID");
		
		HttpServerResponse response = rc.response();
		
		if(badAwRequest(awID, response)) {
			return;
		}
		
		String queueId = rc.request().getParam("queueID");

		if (queueId == null) {
			sendError(400, response);
		} else {
			Message m = QueuesManager.consumeMessage(queueId);
			
			if(m != null) {
				JsonObject messageObj = new JsonObject()
						.put("timestamp", m.timestamp())
						.put("type", m.type())
						.put("content", m.content());
				
				response.putHeader("content-type", "application/json").end(messageObj.encodePrettily());
			} else {
				sendError(400, response);
			}
		}
	}
	
	protected static void handlePushMessageToQueue(RoutingContext rc) {
		String awID = rc.request().getParam("awID");
		
		HttpServerResponse response = rc.response();
		
		if(badAwRequest(awID, response)) {
			return;
		}
		
		String queueId = rc.request().getParam("queueID");

		if (queueId == null) {
			sendError(400, response);
		} else {
			JsonObject messageObj = rc.getBodyAsJson();
			
			QueuesManager.storeMessage(queueId, 
					new Message(messageObj.getString("type"), messageObj.getJsonObject("content")));
			
			response.end();
		}
	}
	
	public static void handleAgentRegisterPolicy(RoutingContext rc) {
		String awID = rc.request().getParam("awID");
		
		HttpServerResponse response = rc.response();
		
		if(badAwRequest(awID, response)) {
			return;
		}
		
		String agentName = rc.request().getParam("agentName");
		
		if(agentName == null) {
			sendError(400, response);
			return;
		}
		
		JsonObject policy = rc.getBodyAsJson();
		
		if(policy != null) {
			if(agentName.equals(CollisionDetectorAgent.class.getSimpleName())) {
				JsonObject responseObj;
				try {
					responseObj = CollisionDetectorAgent.instance().registerNewPolicy(policy);
				} catch (PolicyMalformedException e) {
					sendError(400, response);
					return;
				}
				response.putHeader("content-type", "application/json").end(responseObj.encodePrettily());
				return;
			}
			
			sendError(400, response);
		} else {
			sendError(400, response);
		}
	}
	
	public static void handleAgentRemovePolicy(RoutingContext rc) {
		String awID = rc.request().getParam("awID");
		
		HttpServerResponse response = rc.response();
		
		if(badAwRequest(awID, response)) {
			return;
		}
		
		String agentName = rc.request().getParam("agentName");
		String policyId = rc.request().getParam("policyID");
		
		if(agentName == null) {
			sendError(400, response);
			return;
		}
		
		if(policyId != null) {
			if(agentName.equals(CollisionDetectorAgent.class.getSimpleName())) {
				CollisionDetectorAgent.instance().removePolicy(policyId);
				response.end();
				return;
			}
			
			sendError(400, response);
		} else {
			sendError(400, response);
		}
	}
	
	/*
	 * PRIVATE MEMBERS
	 */
	
	private static boolean checkRequestBody(String requestName, JsonObject body) {
		switch(requestName) {
			case "createAw":
				return body.containsKey("environment") 
						&& (body.getString("environment").equals("vr") || body.getString("environment").equals("ar"))
						&& body.containsKey("reference-system")
						&& (body.getString("reference-system").equals("default") || body.getString("reference-system").equals("marker-based"))
						&& (body.getString("reference-system").equals("marker-based") ? body.containsKey("marker-db") : true);
			
			default:
				return false;
		}
	}
	
	private static boolean badAwRequest(String awID, HttpServerResponse response) {
		if (awID == null) {
			sendError(HttpStatus.BAD_REQUEST, response);
			return true;
		}
		
		if(!AW.instance().isActive() || !AW.instance().name().equals(awID)) {
			sendError(HttpStatus.NOT_FOUND, response);
			return true;
		}
		
		return false;
	}
	
	private static void sendOK(HttpServerResponse response) {
		response
			.setStatusCode(HttpStatus.OK)
			.end();
	}
	
	private static void sendOK(String responseBody, HttpServerResponse response) {
		response
		.setStatusCode(HttpStatus.OK)
		.putHeader("content-type", "application/json")
		.end(responseBody);
	}
	
	private static void sendOK(JsonObject responseBody, HttpServerResponse response) {
		response
			.setStatusCode(HttpStatus.OK)
			.putHeader("content-type", "application/json")
			.end(responseBody.encodePrettily());
	}
	
	private static void sendError(int statusCode, HttpServerResponse response) {
		sendError(statusCode, null, response);
	}

	private static void sendError(int statusCode, String statusMessage, HttpServerResponse response) {
		response.setStatusCode(statusCode);
		
		if(statusMessage != null){
			response.setStatusMessage(statusMessage);
		}
		
		response.end();
	}

	private static void log(String msg) {
		LoggerFactory.getLogger(AWServiceHandlers.class).info("[" + AWService.class.getSimpleName() + "] " + msg);
	}
}
