package it.unibo.disi.pslab.aw.ontology;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import it.unibo.disi.pslab.aw.Config;
import it.unibo.disi.pslab.aw.annotations.PROPERTY;
import it.unibo.disi.pslab.aw.net.UnityEngine;
import it.unibo.disi.pslab.aw.ontology.ae.Extension;
import it.unibo.disi.pslab.aw.ontology.ae.Location;
import it.unibo.disi.pslab.aw.ontology.ae.Orientation;
import it.unibo.disi.pslab.aw.ontology.exceptions.ActionNotFoundException;
import it.unibo.disi.pslab.aw.ontology.exceptions.PropertyNotFoundException;
import it.unibo.disi.pslab.aw.ontology.interfaces.JsonFormattable;

public class AE implements JsonFormattable{
		
	private String id;
	private String tag;
	private String type;
	
	private Location location;
	private Orientation orientation;
	private Extension extension;
	private JsonArray holograms;
	private JsonArray things;
	
	private Map<String, Object> customProperties = new HashMap<>();
	private Map<String, String[]> actions = new HashMap<>();
	
	/*
	 * MODEL
	 */
	
	public final String id() {
		return this.id;
	}
	
	public final String tag() {
		return this.tag;
	}

	public final String type() {
		return this.type;
	}

	/*
	 * PROPERTIES
	 */
	
	//GETTERS
	
	public final Location location() {
		return this.location;
	}
	
	public final Orientation orientation() {
		return this.orientation;
	}
	
	public final Extension extension() {
		return this.extension;
	}
	
	public final JsonArray holograms() {
		return this.holograms;
	}
	
	public final JsonArray things() {
		return this.things;
	}
	
	public final Set<String> customProperties(){
		return customProperties.keySet();
	}
	
	public final Object customProperty(String name) throws PropertyNotFoundException{		
		if(!customProperties.containsKey(name)) {
			throw new PropertyNotFoundException(); 
		}
		
		return customProperties.get(name);
	}
	
	//SETTERS
	
	public final void location(Location location) {
		this.location = location;
		
		if(UnityEngine.isEnabled()) {
			UnityEngine.updateHologramProperty(id, "location", location.getJSONRepresentation());
		}
	}
	
	public final void orientation(Orientation orientation) {
		this.orientation = orientation;
		
		if(UnityEngine.isEnabled()) {
			UnityEngine.updateHologramProperty(id, "orientation", orientation.getJSONRepresentation());
		}
	}

	public final void extension(Extension extension) {
		this.extension = extension;
		
		if(UnityEngine.isEnabled()) {
			UnityEngine.updateHologramProperty(id, "extension", extension.getJSONRepresentation());
		}
	}

	public final void holograms(JsonArray holograms) {
		this.holograms = holograms;
	}
	
	public final void things(JsonArray things) {
		this.things = things;
	}
	
	public final void customProperty(String name, Object newValue) throws PropertyNotFoundException{		
		if(!customProperties.containsKey(name)) {
			throw new PropertyNotFoundException();
		}
		
		customProperties.replace(name, newValue);
		
		try {
			Field f = getClass().getDeclaredField(name);
			f.setAccessible(true);
			if(f.isAnnotationPresent(PROPERTY.class) && !f.getAnnotation(PROPERTY.class).onUpdate().equals("")) {
				Method m = getClass().getDeclaredMethod(f.getAnnotation(PROPERTY.class).onUpdate());
				m.setAccessible(true);
				m.invoke(this);
			}
		} catch (NoSuchFieldException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException e) {
			e.printStackTrace();
		}
		
		String representation = "";

		try {
			representation = JsonObject.mapFrom(customProperties.get(name)).encodePrettily();
		} catch (IllegalArgumentException e) {
				representation = String.valueOf(customProperties.get(name));
		}
		
		if(UnityEngine.isEnabled()) {
			UnityEngine.updateHologramProperty(id, name, representation);
		}
	}
	
	/*
	 * ACTIONS
	 */
	
	public final Map<String, String[]> actions(){
		return this.actions;
	}
	
	public final <T extends AE>void executeAction(T ae, String name, Object...params) throws ActionNotFoundException {
		if(!actions.keySet().contains(name)) {
			throw new ActionNotFoundException();
		}
		
		try {
			new Thread(() -> {
				try {
					Class<?> c = Class.forName(Config.ENTITIES_PATH + "." + type);
					
					if(params.length != 0) {
						String[] paramsTypeList = actions.get(name);
						Class<?>[] paramsClassList = new Class<?>[paramsTypeList.length];
						
						for(int i = 0; i < paramsTypeList.length; i++) {
							paramsClassList[i] = Class.forName(paramsTypeList[i]);
						}
						
						Method m = c.getMethod(name, paramsClassList);
						m.invoke(ae, params);
						
					} else {
						Method m = c.getMethod(name);
						m.invoke(ae);
					}
				} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | ClassNotFoundException | NoSuchMethodException | SecurityException e) {
					System.out.println("Error in invoke method!");
					e.printStackTrace();
				}
			}).start();
			
		} catch (IllegalArgumentException | SecurityException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * @see it.unibo.disi.pslab.aw.ontology.interfaces.JsonFormattable#getJSONRepresentation()
	 */
	public final JsonObject getJSONRepresentation() {
		JsonObject propertiesObject =  new JsonObject();
		
		propertiesObject.put("location", location.getJSONRepresentation());
		propertiesObject.put("orientation", orientation.getJSONRepresentation());
		propertiesObject.put("extension", extension.getJSONRepresentation());
		propertiesObject.put("holograms", holograms);
		
		customProperties.entrySet().forEach(p -> {
			try {
				propertiesObject.put(p.getKey(), p.getValue());
			} catch(IllegalStateException e) {
				propertiesObject.put(p.getKey(), JsonObject.mapFrom(p.getValue()));
			}
		});
		
		JsonArray actionArray = new JsonArray();
		
		actions.keySet().forEach(action -> {
			JsonArray types = new JsonArray();
			
			for(int i = 0; i < actions.get(action).length; i++) {
				types.add(actions.get(action)[i]);
			}
			
			actionArray.add(new JsonObject()
					.put("name", action)
					.put("paramsTypeList", types));
		});
		
		
		return new JsonObject()
				.put("model", new JsonObject()
						.put("id", id)
						.put("tag", tag)
						.put("type", type))
				.put("properties", propertiesObject)
				.put("things", things)
				.put("actions", actionArray);
	}
	
	protected static void log(AE instance, String msg) {
		System.out.println("[" + instance.id + ":"+ instance.getClass().getSimpleName() +"] " + msg);
	}
}