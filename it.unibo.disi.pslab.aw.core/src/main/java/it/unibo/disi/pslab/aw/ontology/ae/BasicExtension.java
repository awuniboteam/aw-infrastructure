package it.unibo.disi.pslab.aw.ontology.ae;

import io.vertx.core.json.JsonObject;

public class BasicExtension extends Extension {

	private double radius;
	
	public BasicExtension(double radius) {
		super(Extension.Type.BASIC);
		
		this.radius = radius;
	}
	
	public double radius() {
		return this.radius;
	}

	@Override
	public JsonObject getJSONRepresentation() {
		return new JsonObject()
			.put("type", type.toString())
			.put("radius", radius);
	}
}
