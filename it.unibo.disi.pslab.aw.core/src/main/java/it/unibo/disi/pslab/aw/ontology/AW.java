package it.unibo.disi.pslab.aw.ontology;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.vertx.core.json.JsonObject;
import it.unibo.disi.pslab.aw.infrastructure.MessageQueue;
import it.unibo.disi.pslab.aw.net.UnityEngine;
import it.unibo.disi.pslab.aw.ontology.exceptions.AugmentedEntityNotFoundException;
import it.unibo.disi.pslab.aw.ontology.exceptions.AugmentedWorldAlreadyStartedException;
import it.unibo.disi.pslab.aw.ontology.interfaces.JsonFormattable;

public class AW implements JsonFormattable{
	
	/*
	 * STATIC MEMBERS
	 */
	
	private static AW instance = new AW();
	
	/**
	 * 
	 * @return
	 */
	public static AW instance() {		
		return instance;
	}
	
	/*
	 * NON-STATIC MEMBERS
	 */
	
	private volatile boolean active;

	private String name;
	private JsonObject details;
	private Date startDateTime;
	
	private List<AE> entities;
	
	private List<MessageQueue> messageQueues;
	
	private AW() {
		this.active = false;
		
		this.entities = new ArrayList<>();
		this.messageQueues = new ArrayList<>();
	}
	
	/**
	 * 
	 * @param name
	 * @param details
	 * @throws AugmentedWorldAlreadyStartedException
	 */
	public void start(String name, JsonObject details) throws AugmentedWorldAlreadyStartedException{
		if(!active) {
			this.active = true;
			
			this.name = name;
			this.details = details;
			this.startDateTime = new Date();
			
			if(UnityEngine.isEnabled()) {
				UnityEngine.istantiateAugmentWorld(name, details);
			}
		} else {
			throw new AugmentedWorldAlreadyStartedException();
		}
	}
	
	/**
	 * 
	 * @return
	 */
	public boolean isActive() {
		return this.active;
	}
	
	/**
	 * 
	 * @return
	 */
	public String name() {
		return this.name;
	}
	
	/**
	 * 
	 * @return
	 */
	public List<AE> entities() {
		return this.entities;
	}
	
	/**
	 * 
	 * @param id
	 * @return
	 * @throws AugmentedEntityNotFoundException
	 */
	public AE entity(String id) throws AugmentedEntityNotFoundException{
		for(AE ae : entities) {
			if(ae.id().equals(id)) {
				return ae;
			}
		}
		
		throw new AugmentedEntityNotFoundException();
	}
	
	/**
	 * Adds a new Augmented Entity to the current instance of the AW.
	 * @param ae The entity to be added.
	 */
	public void addAugmentedEntity(AE ae) {
		entities.add(ae);
		
		if(UnityEngine.isEnabled()) {
			UnityEngine.createHologram(ae);
		}
	}
	
	/**
	 * Removes a previously added Augmented Entity from the current instance of the AW.
	 * @param id The identifier of the entity to be removed.
	 */
	public void removeAugmentedEntity(String id) throws AugmentedEntityNotFoundException{
		
		entities.forEach(ae -> {
			if(ae.id().equals(id)) {
				entities.remove(ae);
				
				if(UnityEngine.isEnabled()) {
					UnityEngine.removeHologram(id);
				}
				
				return;
			}
		});
		
		throw new AugmentedEntityNotFoundException();
	}
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	public boolean checkEntityAvailability(String id) {
		for(AE ae : entities) {
			if(ae.id().equals(id)) {
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * 
	 * @return
	 */
	public List<MessageQueue> messageQueues() {
		return this.messageQueues;
	}

	@Override
	public JsonObject getJSONRepresentation() {
		return new JsonObject()
			.put("name", name)
			.put("startDate", new SimpleDateFormat("yyyy-MM-dd").format(startDateTime))
			.put("startTime", new SimpleDateFormat("hh:mm:ss").format(startDateTime))
			.put("details", details);
	}
}
