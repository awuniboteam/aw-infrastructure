package it.unibo.disi.pslab.aw.ontology.ae;

import it.unibo.disi.pslab.aw.ontology.interfaces.JsonFormattable;

public abstract class Location implements JsonFormattable {
	
	protected Type type;
	
	public Location(Type type) {
		this.type = type;
	}
	
	public Type type() {
		return type;
	}
	
	@Override
	public String toString() {
		return "location";
	}
	
	public enum Type {
		CARTESIAN("3D/cartesian-location"),
		GPS("gps-location");
		
		private String rep;
		
		Type(String rep) {
			this.rep = rep;
		}
		
		@Override
		public String toString() {
			return rep;
		}
	}
}
