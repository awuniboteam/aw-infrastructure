package it.unibo.disi.pslab.aw.ontology.ae;

import io.vertx.core.json.JsonObject;

public class CartesianLocation extends Location {

	private double x, y, z;
	
	public CartesianLocation(double x, double y, double z) {
		super(Location.Type.CARTESIAN);
		
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public double x() {
		return x;
	}
	
	public double y() {
		return y;
	}
	
	public double z() {
		return z;
	}

	@Override
	public JsonObject getJSONRepresentation() {
		return new JsonObject()
			.put("type", type.toString())
			.put("x", x)
			.put("y", y)
			.put("z", z);
	}
}
