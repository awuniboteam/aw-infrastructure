package it.unibo.disi.pslab.aw.infrastructure.agents;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.vertx.core.json.JsonObject;

import it.unibo.disi.pslab.aw.infrastructure.Message;
import it.unibo.disi.pslab.aw.infrastructure.QueuesManager;
import it.unibo.disi.pslab.aw.infrastructure.exceptions.PolicyMalformedException;
import it.unibo.disi.pslab.aw.ontology.AE;
import it.unibo.disi.pslab.aw.ontology.AW;
import it.unibo.disi.pslab.aw.ontology.ae.CartesianLocation;
import it.unibo.disi.pslab.aw.ontology.ae.Location.Type;
import it.unibo.disi.pslab.aw.ontology.ae.SphericExtension;
import it.unibo.disi.pslab.aw.ontology.exceptions.AugmentedEntityNotFoundException;
import it.unibo.disi.pslab.aw.utils.Point3D;

public class CollisionDetectorAgent extends PolicyBasedInfrastructuralAgent{

	private static CollisionDetectorAgent instance = new CollisionDetectorAgent();
	
	public static CollisionDetectorAgent instance() {
		return instance;
	}
		
	@Override
	public synchronized JsonObject registerNewPolicy(JsonObject policy) throws PolicyMalformedException {
		String source = policy.getString("source");
		String target = policy.getString("target");
		String tag = policy.getString("tag");
		Integer delta = policy.getInteger("deltaTime");
		
		if(source == null || target == null || delta == null || !AW.instance().checkEntityAvailability(source)) {
			throw new PolicyMalformedException();
		}
		
		if(target.equals("tag-based") && tag == null) {
				throw new PolicyMalformedException();
		}
		
		String policyId = getNextAvailablePolicyId();
		String queueId = QueuesManager.getNextAvailableQueueId();
		
		executeTask(new DetectCollisionTask(source, target, tag, delta, queueId), policyId);
			
		return new JsonObject()
				.put("policyId", policyId)
				.put("queueId", queueId);
	}
	
	/**
	 * 
	 *
	 */
	class DetectCollisionTask implements AgentTask{
		
		private String source, target, tag, queueId;
		private int deltaTime;
		
		private volatile boolean stop = false;
		
		private Point3D prevSourceCenter = null;
		private Map<String, Point3D> prevTargetsCenter = new HashMap<>();
		
		public DetectCollisionTask(String source, String target, String tag, int delta, String queueId) {
			this.source = source;
			this.target = target;
			this.tag = tag;
			this.deltaTime = delta;
			this.queueId = queueId;
		}

		@Override
		public void run() {
			while(!stop) {
				waitDelta();
			
				if(target.equals("all")) {
					checkCollisionOneToAll(source);
				} else if(target.equals("tag-based")) {
					checkCollisionByTag(source, tag);
				} else {
					checkCollisionOneToOne(source, target);
				}
			}
		}
		
		public void interrupt() {
			stop = true;
		}
				
		private void checkCollisionOneToOne(String sourceId, String targetId) {
			//TODO: source or target could be removed after the task execution. check and in case, remove policy
			
			try {
				AE source = AW.instance().entity(sourceId);
				AE target = AW.instance().entity(targetId);
				
				tryToCheckCollision(source, target);
				
				prevTargetsCenter.put(target.id(), getEntityColliderCenter(target));
				prevSourceCenter = getEntityColliderCenter(source);
				
			} catch (AugmentedEntityNotFoundException e) {
				e.printStackTrace();
			}
		}
		
		private void checkCollisionByTag(String sourceId, String tag) {
			//TODO: source could be removed after the task execution. check and in case, remove policy
			
			try {
				AE source = AW.instance().entity(sourceId);
				List<AE> targets = new ArrayList<>();
				
				AW.instance().entities().forEach(ae -> {
					if(ae.tag().equals(tag)) {
						targets.add(ae);
					}
				});
				
				for(AE target : targets) {
					tryToCheckCollision(source, target);
					prevTargetsCenter.put(target.id(), getEntityColliderCenter(target));
				}
				
				prevSourceCenter = getEntityColliderCenter(source);
			} catch (AugmentedEntityNotFoundException e) {
				e.printStackTrace();
			}
		}
		
		private void checkCollisionOneToAll(String sourceId) {
			//TODO: source could be removed after the task execution. check and in case, remove policy

			try {
				AE source = AW.instance().entity(sourceId);
				List<AE> targets = AW.instance().entities();
				
				for(AE target : targets) {	
					tryToCheckCollision(source, target);
					prevTargetsCenter.put(target.id(), getEntityColliderCenter(target));
				}		
				
				prevSourceCenter = getEntityColliderCenter(source);
			} catch (AugmentedEntityNotFoundException e) {
				e.printStackTrace();
			}
		}
		
		private void waitDelta() {
			try {
				Thread.sleep(deltaTime);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		private Point3D getEntityColliderCenter(AE entity) {
			Type locationType = entity.location().type();
			
			Point3D center = null;
			
			switch(locationType) {
				case CARTESIAN:
					center = new Point3D(
							((CartesianLocation) entity.location()).x(),
							((CartesianLocation) entity.location()).y(),
							((CartesianLocation) entity.location()).z());
					break;
					
				case GPS:
					break;
					
				default:
					break;
			}
			
			return center;
		}
		
		private boolean checkCollision(Point3D p1, double r1, Point3D p2, double r2) {
			double distance = Math.sqrt(
					Math.pow(p1.x() - p2.x(), 2) +
		            Math.pow(p1.y() - p2.y(), 2) +
		            Math.pow(p1.z() - p2.z(), 2));
			
			return distance <= (r1 + r2);
		}
		
		
		
		private void tryToCheckCollision(AE source, AE target) {
			Point3D sourceCenter = getEntityColliderCenter(source);
			Point3D targetCenter = getEntityColliderCenter(target);
			
			boolean sourceMoved = false, targetMoved = false;
			
			if(prevSourceCenter != null) {
				sourceMoved = !sourceCenter.equals(prevSourceCenter);
			}
			
			if(prevTargetsCenter.containsKey(target.id())) {
				targetMoved = !targetCenter.equals(prevTargetsCenter.get(target.id()));
			}
			
			if(sourceMoved || targetMoved) {
				if(checkCollision(sourceCenter, ((SphericExtension)source.extension()).radius(),
						targetCenter, ((SphericExtension)target.extension()).radius())) {
					Message m = new Message("collision-detected", new JsonObject()
							.put("source", source.id())
							.put("target", target.id()));
					
					QueuesManager.storeMessage(queueId, m);
				}
			}
		}
	}
}
