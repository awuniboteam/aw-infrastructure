package it.unibo.disi.pslab.aw.webservices;

public class HttpStatus {
	public static final int OK = 200;
	public static final int BAD_REQUEST = 400;
	public static final int FORBIDDEN = 403;
	public static final int NOT_FOUND = 404;
	public static final int CONFLICT = 409;
}
