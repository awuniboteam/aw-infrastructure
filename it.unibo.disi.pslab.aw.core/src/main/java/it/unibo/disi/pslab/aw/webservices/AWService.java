package it.unibo.disi.pslab.aw.webservices;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.http.HttpServer;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;

public class AWService extends AbstractVerticle {

	private Router router;

	@Override
	public void start(Future<Void> fut) {
		router = Router.router(vertx);
		router.route().handler(BodyHandler.create());

		initResources();

		vertx.executeBlocking(future -> {
			future.complete(vertx.createHttpServer());
		}, res -> {
			log("Server created.");
			vertx.executeBlocking(fut2 -> {
				fut2.complete(((HttpServer) res.result()).requestHandler(router::accept));
			}, res2 -> {
				log("Accept registered.");
				((HttpServer) res.result()).listen(8080, result -> {
					if (result.succeeded()) {
						log("Listening.");
					} else {
						log("Failed: " + result.cause());
						System.exit(0);
					}
				});
			});
		});
	}

	private void initResources() {
		// AW > SERVICE
		router.get("/aw/service/info").handler(AWServiceHandlers::handleGetServiceInfo);

		// AW > ACTIONS
		router.put("/aw/:name").handler(AWServiceHandlers::handleCreateAW);
		router.get("/aw/:name").handler(AWServiceHandlers::handleGetAWInfo);
		
		// AW > INFRASTUCTURAL AGENTS
		router.post("/aw/:awID/agents/:agentName/registerpolicy").handler(AWServiceHandlers::handleAgentRegisterPolicy);
		router.post("/aw/:awID/agents/:agentName/removepolicy/:policyID").handler(AWServiceHandlers::handleAgentRemovePolicy);

		// AW > ENTITIES
		router.get("/aw/:awID/entities").handler(AWServiceHandlers::handleGetEntities);
		router.put("/aw/:awID/entities/:type").handler(AWServiceHandlers::handleAddAugmentedEntity);
		router.get("/aw/:awID/entities/:entityID").handler(AWServiceHandlers::handleGetEntityByID);
		router.delete("/aw/:awID/entities/:entityID").handler(AWServiceHandlers::handleRemoveEntityByID);
		router.get("/aw/:awID/entities/:entityID/model").handler(AWServiceHandlers::handleGetEntityModel);
		router.get("/aw/:awID/entities/:entityID/model/:modelElement").handler(AWServiceHandlers::handleGetEntityModelElement);
		router.get("/aw/:awID/entities/:entityID/properties").handler(AWServiceHandlers::handleGetEntityProperties);
		router.get("/aw/:awID/entities/:entityID/properties/:property").handler(AWServiceHandlers::handleGetEntityProperty);
		router.post("/aw/:awID/entities/:entityID/properties/:property").handler(AWServiceHandlers::handleSetEntityProperty);
		router.post("/aw/:awID/entities/:entityID/properties/holograms/:hologramID/actions/:action").handler(AWServiceHandlers::handleDoActionOnEntityHologram);
		router.get("/aw/:awID/entities/:entityID/actions").handler(AWServiceHandlers::handleGetEntityActions);
		router.post("/aw/:awID/entities/:entityID/actions/:action").handler(AWServiceHandlers::handleDoActionOnEntity);
	
		// AW > MESSAGES
		router.get("/aw/:awID/messages/:queueID").handler(AWServiceHandlers::handleMessageQueueStatus);
		router.get("/aw/:awID/messages/:queueID/pop").handler(AWServiceHandlers::handlePopMessageFromQueue);
		router.put("/aw/:awID/messages/:queueID/push").handler(AWServiceHandlers::handlePushMessageToQueue);
	}
	
	private void log(String msg) {
		LoggerFactory.getLogger(AWService.class).info("[" + AWService.class.getSimpleName() + "] " + msg);
	}
}
