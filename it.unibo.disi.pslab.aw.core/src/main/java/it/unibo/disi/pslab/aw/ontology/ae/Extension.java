package it.unibo.disi.pslab.aw.ontology.ae;

import it.unibo.disi.pslab.aw.ontology.interfaces.JsonFormattable;

public abstract class Extension implements JsonFormattable {

protected Type type;
	
	public Extension(Type type) {
		this.type = type;
	}
	
	public Type type() {
		return type;
	}
	
	@Override
	public String toString() {
		return "extension";
	}
	
	public enum Type {
		BASIC("basic-extension"),
		SPHERIC("spheric-extension");
		
		private String rep;
		
		Type(String rep) {
			this.rep = rep;
		}
		
		@Override
		public String toString() {
			return rep;
		}
	}

}
