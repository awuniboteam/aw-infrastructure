package it.unibo.disi.pslab.aw;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;

import it.unibo.disi.pslab.aw.infrastructure.agents.CollisionDetectorAgent;
import it.unibo.disi.pslab.aw.infrastructure.enumerates.AwEnvironment;
import it.unibo.disi.pslab.aw.infrastructure.enumerates.AwReferenceSystem;
import it.unibo.disi.pslab.aw.infrastructure.exceptions.EnvironmentNotReadyException;
import it.unibo.disi.pslab.aw.net.UnityEngine;
import it.unibo.disi.pslab.aw.webservices.AWService;

public class Environment {
	
	/**
	 * 
	 * @param configFile
	 */
	public static void deploy(String configFile) {
		config(configFile);		
		startAWService();
		startInfrastucturalAgents();
	}
	
	/**
	 * 
	 * @param name
	 * @param environment
	 * @param refSys
	 * @throws EnvironmentNotReadyException
	 */
	public static void runAW(String name, AwEnvironment environment, AwReferenceSystem refSys) throws EnvironmentNotReadyException {
		runAW(name, environment, refSys, null);
	}
	
	/**
	 * 
	 * @param name
	 * @param environment
	 * @param refSys
	 * @param markersDb
	 * @throws EnvironmentNotReadyException
	 */
	public static void runAW(String name, AwEnvironment environment, AwReferenceSystem refSys, String markersDb) throws EnvironmentNotReadyException {
		//TODO: complete the runAW functionality
	}
	
	private static void config(String configFile) {
		//TODO: to be completed
		
		try {
			String configFileContent = new String(Files.readAllBytes(Paths.get(configFile)));
			JsonObject configObj = new JsonObject(configFileContent);
			
			JsonObject awServiceObj = configObj.getJsonObject("aw-service");
			
			/*
			 * LOCAL SERVICE OBJ
			 */
			JsonObject localServiceObj = awServiceObj.getJsonObject("local-service");
			
			/*
			 * REMOTE UNITY ENGINE OBJ
			 */
			JsonObject remoteUnityEngineObj = awServiceObj.getJsonObject("remote-unity-engine");
			
			if(remoteUnityEngineObj != null && remoteUnityEngineObj.getBoolean("enabled") != null) {
				if(remoteUnityEngineObj.getBoolean("enabled")) {
					Config.UNITY_ENGINE_ENABLED = true;
					Config.UNITY_ENGINE_ADDRESS = remoteUnityEngineObj.getString("ip");
					Config.UNITY_ENGINE_PORT = remoteUnityEngineObj.getInteger("port");
					Config.UNITY_ENGINE_SEND_ATTEMPTS = remoteUnityEngineObj.getInteger("send-attempts");
				} else {
					Config.UNITY_ENGINE_ENABLED = false;
				}
			} else {
				System.out.println("Configuration file does not have a definition for the unity remote engine settings.");
			}

			/*
			 * AE SRC PATH
			 */
			Config.ENTITIES_PATH = awServiceObj.getString("ae-src-path");			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private static void startAWService() {
		try {
			Vertx vertx = Vertx.vertx();
			vertx.deployVerticle(new AWService());
			
			if(Config.UNITY_ENGINE_ENABLED) {
				UnityEngine.init("aw-service", vertx);
			}
		} catch (Exception ex){
			ex.printStackTrace();
		}
	}
	
	private static void startInfrastucturalAgents() {
		CollisionDetectorAgent.instance().start();
	}
}
